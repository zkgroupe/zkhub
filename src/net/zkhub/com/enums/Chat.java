package net.zkhub.com.enums;

public enum Chat {
	
	MUTED(false),
	NORMAL(true),
	SLOW(false);
	
	private boolean canChat;
	private static Chat current;
	
	Chat(boolean a){
		canChat = a;
	}
	
	public static void setChatMode(Chat mode){
		current = mode;
	}
	
	public static boolean isState(Chat mode){
		return current == mode;
	}
	
	public static Chat getChatMode(){
		return current;
	}
}
