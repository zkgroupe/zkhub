package net.zkhub.com.enums;

public enum Jeux {
	
	ZKPVP("ZK PvP"),
	ZKGAMES("ZK Games"),
	ZKFACTION("ZK Faction"),
	ZKSERV("ZK Serv");
	
	private String name;
	
	Jeux(String name){
		this.name = name;
	}
	
	public static String getName(Jeux g){
		return g.name;
	}
	
	public static String setName(Jeux g){
		return g.name;
	}
}


