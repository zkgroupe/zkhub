package net.zkhub.com.enums;

public enum Events {

	Join("Join"),
	Quit("Quit"),
	Load("Load");
	
	private String events;
	
	Events(String events){
		this.events = events;
	}
	
	public static String getEvents(Events events){
		return events.events;
	}
	
}
