package net.zkhub.com.gui.boutique;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class BoutiqueGUI implements Listener{
	
	public static void openBoutique(Player p){
		
		Inventory inv = Bukkit.createInventory(null, 9 * 3, "Boutique");
		
		ItemStack vitre = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 1);
		ItemMeta vitrem = vitre.getItemMeta();
		vitrem.setDisplayName(ChatColor.BOLD + "");
		vitre.setItemMeta(vitrem);
		for (int i = 0; i < 27; i++) {
			if (!(i >= 12 && i <= 14) || !(i == 13)) {
				inv.setItem(i, vitre);
			}
		}
		
		ItemStack blaze = new ItemStack(Material.BLAZE_POWDER, 1);
		ItemMeta blazeM = blaze.getItemMeta();
		blazeM.setDisplayName("�6Particules");
		ArrayList<String> lore = new ArrayList<>();
		lore.add("�b�oTiens, tiens une particule �a vous dit ?");
		blazeM.setLore(lore);
		blaze.setItemMeta(blazeM);
		inv.setItem(13, blaze);
		
		p.openInventory(inv);
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent event){
		Player p = (Player) event.getWhoClicked();

		if (event.getCurrentItem() == null || event.getCurrentItem().getType() == Material.AIR
				|| !event.getCurrentItem().hasItemMeta()) {
			return;
		}
		
		if(p.getOpenInventory().getTitle().equalsIgnoreCase("Boutique")){
			switch (event.getCurrentItem().getType()) {
			case BLAZE_POWDER:
				ParticleGUI.openGui(p);
				break;

			default:
				break;
			}
			
		}
	}

}
