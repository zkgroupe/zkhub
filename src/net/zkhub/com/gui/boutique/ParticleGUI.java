package net.zkhub.com.gui.boutique;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.minecraft.server.v1_8_R3.EnchantmentSlotType;
import net.minecraft.server.v1_8_R3.EnumParticle;
import net.zkhub.com.boutique.Particle;
import net.zkhub.com.boutique.ParticleEffect;
import net.zkhub.com.utils.ArrayLists;

public class ParticleGUI implements Listener {

	public ItemStack createItem(Material material, String displayName, ArrayList<String> lore, boolean isUsed){

		ItemStack item = new ItemStack(material);
		ItemMeta item_meta = item.getItemMeta();
		item_meta.setDisplayName(displayName);
		item_meta.setLore(lore);		
		if(isUsed == true){

			item_meta.addEnchant(Enchantment.DAMAGE_ALL, 0, isUsed);

		}
		return item;

	}

	public static void openGui(Player p){

		Inventory gui = Bukkit.createInventory(null, 3*9, "Boutique > Particules");

		ItemStack retour = new ItemStack(Material.FEATHER, 1);
		ItemMeta retourM = retour.getItemMeta();
		retourM.setDisplayName("�fRetour en arri�re");
		retour.setItemMeta(retourM);
		gui.setItem(26, retour);
		
		ItemStack lava_particle = new ItemStack(Material.LAVA_BUCKET, 1);
		ItemMeta lava_particle_meta = lava_particle.getItemMeta();
		lava_particle_meta.setDisplayName("�6Particule LAVE");

		if(ArrayLists.particle.containsKey(p)){

			if(ArrayLists.particle.containsValue(EnumParticle.LAVA)){

				lava_particle_meta.addEnchant(Enchantment.DAMAGE_ALL, 1, false);

			}

		}

		lava_particle.setItemMeta(lava_particle_meta);

		gui.setItem(5, lava_particle);

		p.openInventory(gui);


	}

	@EventHandler
	public void onClick(InventoryClickEvent e){

		Player p = (Player) e.getWhoClicked();

		if (e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR
				|| !e.getCurrentItem().hasItemMeta()) {
			return;
		}

		if(p.getOpenInventory().getTitle().equalsIgnoreCase("Boutique > Particules")){
			switch (e.getCurrentItem().getType()) {
			case LAVA_BUCKET:
				Particle.setPlayerParticle(p, ParticleEffect.LAVA);
				p.closeInventory();
				break;
			case FEATHER:
				BoutiqueGUI.openBoutique(p);
				break;
			default:
				break;
			}
		}	
	}	
}
