package net.zkhub.com.gui;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class ProfilGUI implements Listener{
	
    public static void openProfil(Player p){
		
    	Inventory inv = Bukkit.createInventory(null, 9 * 3, "Profil");
    	
    	ItemStack vitre = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 13);
		ItemMeta vitrem = vitre.getItemMeta();
		vitrem.setDisplayName(ChatColor.BOLD + "");
		vitre.setItemMeta(vitrem);
		for (int i = 0; i < 27; i++) {
			if (!(i >= 12 && i <= 14) || !(i == 13)) {
				inv.setItem(i, vitre);
			}
		}
		
		 ItemStack skull = null;
	        for (Player pl : Bukkit.getOnlinePlayers()) {
	            skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
	            SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
	            skullMeta.setDisplayName("�6�o" + p.getDisplayName());
	            skullMeta.setOwner(p.getDisplayName());
	            ArrayList<String> lore = new ArrayList<>();
	            
	            lore.add("");
	            lore.add("Test");
	            lore.add("");
	            
	            skullMeta.setLore(lore);
	            skull.setItemMeta(skullMeta);
	            inv.setItem(13, skull);
	        }
    	
    	p.openInventory(inv);
    	
	}
    
    public void onClickInventory(InventoryClickEvent e){
    	Player p = (Player) e.getWhoClicked();

		if (e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR
				|| !e.getCurrentItem().hasItemMeta()) {
			return;
		}
		
		if(p.getOpenInventory().getTitle().equalsIgnoreCase("Profil")){
			switch (e.getCurrentItem().getType()) {
			case SKULL_ITEM:
				e.setCancelled(true);
				break;
			default:
				break;
			}
		}
    }
}
