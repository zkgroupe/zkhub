package net.zkhub.com.gui;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.zkhub.com.waiting.WaitingUtils;

public class GameGUI implements Listener{

	public static void openMenu(Player p){

		Inventory inv1 = Bukkit.createInventory(null, 9 * 3, "Menu Principal");

		ItemStack vitre = new ItemStack(Material.STAINED_GLASS_PANE, 1);
		ItemMeta vitrem = vitre.getItemMeta();
		vitrem.setDisplayName(ChatColor.BOLD + "");
		vitre.setItemMeta(vitrem);
		for (int i = 9; i < 26; i++) {
			if (!(i >= 12 && i <= 14) || !(i == 18)) {
				inv1.setItem(i, vitre);
			}
		}
		
		ItemStack LiensUtiles = new ItemStack(Material.SIGN, 1);
		ItemMeta LiensUtilesM = LiensUtiles.getItemMeta();
		LiensUtilesM.setDisplayName(ChatColor.BOLD + "§6Liens Utiles");
		LiensUtiles.setItemMeta(LiensUtilesM);
		inv1.setItem(18, LiensUtiles);

		ItemStack Stats = new ItemStack(Material.PAPER, 1);
		ItemMeta StatsM = Stats.getItemMeta();
		StatsM.setDisplayName(ChatColor.BOLD + "§cStatistiques");
		Stats.setItemMeta(StatsM);
		inv1.setItem(26, Stats);

		ItemStack SheepWars = new ItemStack(Material.GOLD_CHESTPLATE, 1);
		ItemMeta SheepWarsM = SheepWars.getItemMeta();
		SheepWarsM.setDisplayName(ChatColor.BOLD + "§e§lZk§6§lPvp");
		SheepWars.setItemMeta(SheepWarsM);
		inv1.setItem(1, SheepWars);

		ItemStack UhcRun = new ItemStack(Material.TNT, 1);
		ItemMeta UhcRunM = UhcRun.getItemMeta();
		UhcRunM.setDisplayName(ChatColor.BOLD + "§e§lZk§6§lFaction");
		UhcRun.setItemMeta(UhcRunM);
		inv1.setItem(3, UhcRun);

		ItemStack SkyDefender = new ItemStack(Material.ARROW, 1);
		ItemMeta SkyDefenderM = SkyDefender.getItemMeta();
		SkyDefenderM.setDisplayName(ChatColor.BOLD + "§e§lZk§6§lGames");
		SkyDefender.setItemMeta(SkyDefenderM);
		inv1.setItem(5, SkyDefender);
		
		ItemStack zkserv = new ItemStack(Material.GOLD_BLOCK, 1);
		ItemMeta zkservm = zkserv.getItemMeta();
		zkservm.setDisplayName(ChatColor.BOLD + "§e§lZk§6§lServ");
		zkserv.setItemMeta(zkservm);
		inv1.setItem(7, zkserv);

		p.openInventory(inv1);
		p.playSound(p.getLocation(), Sound.SUCCESSFUL_HIT, 1, 1);
	}


	@EventHandler
    public void onClickCompassInv(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();

		if (e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR
				|| !e.getCurrentItem().hasItemMeta()) {
			return;
		}

		// Verification de l'inentaire
		if (p.getOpenInventory().getTitle().equalsIgnoreCase("Menu Principal")) {
			switch (e.getCurrentItem().getType()) {
			case GOLD_CHESTPLATE:
				WaitingUtils.sendServer(p, "zkpvp");
				e.setCancelled(true);
			case SIGN:
				p.closeInventory();
				p.sendMessage("§f§l§m-----§r§a§l§m----------§r§2§l§m-----------------§r§a§l§m----------§r§f§l§m-----");
				p.sendMessage("");
			    p.sendMessage("§6Site internet :§7 www.groupezk.com");
		        p.sendMessage("§6TeamSpeak :§7 ts.groupezk.com");
			    p.sendMessage("§6Twitter : §7@GroupeZK");
	      	    p.sendMessage("");
				p.sendMessage("§f§l§m-----§r§a§l§m----------§r§2§l§m-----------------§r§a§l§m----------§r§f§l§m-----");
				break;
			default:
				break;
			}
		}
	}

}
