package net.zkhub.com.listeners;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class Quit implements Listener{

	@EventHandler 
	public void onQuit(PlayerQuitEvent e){

		Player p = e.getPlayer();

		for(Player pl : Bukkit.getOnlinePlayers()){
			if(pl.hasPermission("staff")){
				if(p.hasPermission("staff")){
					pl.sendMessage("§f§l§m-----§r§a§l§m----------§r§2§l§m-----------------§r§a§l§m----------§r§f§l§m-----");
					pl.sendMessage(" ");
					pl.sendMessage("§2 " + p.getName() + " §6§os'est déconnecté.");
					pl.sendMessage(" ");
					pl.sendMessage("§f§l§m-----§r§a§l§m----------§r§2§l§m-----------------§r§a§l§m----------§r§f§l§m-----");
				}
			}
		}
		e.setQuitMessage(null);
	}
}
