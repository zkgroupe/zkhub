package net.zkhub.com.listeners;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitTask;

import com.comphenix.protocol.ProtocolLibrary;

import net.zkhub.com.Hub;
import net.zkhub.com.cmd.VanishCMD;
import net.zkhub.com.nick.NMSUtils;
import net.zkhub.com.utils.MessagesUtils;
import net.zkhub.com.utils.SendTitle;

public class Join implements Listener{
	
	private Player p;
	private BukkitTask task;
	private int timer = 600;
	
	@EventHandler
    public void onJoin(PlayerJoinEvent e) {

		p = e.getPlayer();
		
        initPlayer(p);
        
        Object handle = NMSUtils.getHandle(p);
        Object profile = NMSUtils.getMethod(CraftPlayer.class, "getProfile");

        //Type PERM
        if (p.isOp() || p.hasPermission("admin")) {
            e.setJoinMessage(ChatColor.RED + "[Administrateur]" + MessagesUtils.espace + p.getName() + ChatColor.GOLD + ChatColor.ITALIC + " a rejoint le lobby !");
            p.setPlayerListName(ChatColor.RED + "[Admin] " + p.getName() + " §7[§a✔§7]");
        } else if (p.hasPermission("mod")) {
            e.setJoinMessage(ChatColor.GREEN + "[Modérateur]" + MessagesUtils.espace + p.getName() + ChatColor.GOLD + ChatColor.ITALIC + " a rejoint le lobby !");            
            p.setPlayerListName(ChatColor.GREEN + "[Modérateur] " + p.getName() + " §7[§a✔§7]");
        } else if (p.hasPermission("respmod")) {
            e.setJoinMessage(ChatColor.DARK_GREEN + "[Resp.Mod]" + MessagesUtils.espace + p.getName() + ChatColor.GOLD + ChatColor.ITALIC + " a rejoint le lobby !");            
            p.setPlayerListName(ChatColor.DARK_GREEN + "[RespMod] " + p.getName() + " §7[§a✔§7]");
        } else if (p.hasPermission("Ami")) {
            e.setJoinMessage(ChatColor.WHITE + "Ami §3§f " + p.getName() + ChatColor.GOLD + ChatColor.ITALIC + " a rejoint le lobby !");     
            p.setPlayerListName(ChatColor.WHITE + "Ami §3§f " + p.getName());
        } else if (p.hasPermission("dev")) {
            e.setJoinMessage(ChatColor.YELLOW + "[Développeur]" + MessagesUtils.espace + p.getName() + ChatColor.GOLD + ChatColor.ITALIC + " a rejoint le lobby !");
            p.setPlayerListName(ChatColor.YELLOW + "[Dev] " + p.getName() + " §7[§a✔§7]");

        } else {
            e.setJoinMessage(null);
            p.setPlayerListName(ChatColor.GRAY + p.getName());
            p.setDisplayName(p.getCustomName());
        }
    }
	
	private void initPlayer(Player p){
		//Boussole Slot0
        ItemStack Compass0 = new ItemStack(Material.COMPASS);
        ItemMeta CompassMETA = Compass0.getItemMeta();
        CompassMETA.setDisplayName("§aMenu Principal §7(Clique-Droit)");
        Compass0.setItemMeta(CompassMETA);

        ItemStack skull = null;
        for (Player pl : Bukkit.getOnlinePlayers()) {
            skull = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
            SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
            skullMeta.setDisplayName("§cProfil §7(Clique-Droit)");
            skullMeta.setOwner(pl.getName());
            skull.setItemMeta(skullMeta);
        }
        //Boutique Slot5

        ItemStack Boutique4 = new ItemStack(Material.GOLD_INGOT);
        ItemMeta BoutiqueMeta = Boutique4.getItemMeta();
        BoutiqueMeta.setDisplayName("§6Boutique §7(Clique-Droit)");
        Boutique4.setItemMeta(BoutiqueMeta);

        //Montures Slot9
        ItemStack Montures9 = new ItemStack(Material.ANVIL);
        ItemMeta MontureMETA = Montures9.getItemMeta();
        MontureMETA.setDisplayName("§5Cosmétiques §7(Clique-Droit)");
        Montures9.setItemMeta(MontureMETA);

        p.setGameMode(GameMode.ADVENTURE);
        p.getInventory().setArmorContents(new ItemStack[0]);
        SendTitle.sendTitle(p, "§6GroupeZK", "§fEnfin le retour du Pvp.", 60);
        Location spawn = new Location(Bukkit.getWorlds().get(0), 150, 179, -127);
        p.teleport(spawn);
        p.getInventory().clear();
        p.getInventory().setItem(0, Compass0);
        p.getInventory().setItem(1, skull);
        p.getInventory().setItem(7, Boutique4);
        p.getInventory().setItem(8, Montures9);
        
        p.sendMessage("");
        p.sendMessage("");
        p.sendMessage("§fBienvenue à toi sur les §6GroupesZK§f, §7 " + p.getName());
        p.sendMessage("");
        p.sendMessage("§f§l§m-----§r§e§l§m----------§r§6§l§m-----------------§r§e§l§m----------§r§f§l§m-----");
        p.sendMessage("");
        if (p.isOp() || p.hasPermission("admin")) {
        	p.sendMessage("§fGrade : §cAdministrateur");
        } else if (p.hasPermission("mod")) {
        	p.sendMessage("§fGrade : §2Modérateur");
        } else if (p.hasPermission("respmod")) {
        	p.sendMessage("§fGrade : §cResp.Mod");
        } else if (p.hasPermission("Ami")) {
        	p.sendMessage("§fGrade : §fAmi ✮");
        } else if (p.hasPermission("dev")) {
        	p.sendMessage("§fGrade : §eDéveloppeur");
        } else {
        	p.sendMessage("§fGrade : §7Joueur");
        }
        p.sendMessage("");
        p.sendMessage("Coins : §c§l0");
        p.sendMessage("");
        if(ProtocolLibrary.getProtocolManager().getProtocolVersion(p) == 47){
        	p.sendMessage("Version : §61.8 §eà §61.8.9");
        	p.sendMessage("");
        	p.sendMessage("§cAttention : §6Nous vous conseillons d'utiliser la version §e1.10 §6ou §e1.9§6.");
        }else{
        	p.sendMessage("Version : §c§lInconnue.");
        }
        p.sendMessage("");
        p.sendMessage("Joueurs en ligne : §e§l" + Bukkit.getOnlinePlayers().size());
        p.sendMessage("");
        p.sendMessage("§f§l§m-----§r§e§l§m----------§r§6§l§m-----------------§r§e§l§m----------§r§f§l§m-----");
        p.sendMessage("");
        p.sendMessage("");
	}
	
}
