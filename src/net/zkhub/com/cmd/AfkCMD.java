package net.zkhub.com.cmd;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.zkhub.com.Hub;
import net.zkhub.com.utils.ArrayLists;


public class AfkCMD implements CommandExecutor{
	
	private static final String HELP_MESSAGE = "§cErreur : "
			+ "- AFK §aON/§cOFF"
			+ "- TabList : §a✔/§c✘";

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)){
			Hub.log("Command executable only by players");
			return true;
		}
		
		Player p = (Player)sender;
		
		if(!(p.hasPermission("mod") || p.isOp())) return true;

		if(args.length == 0){

			if(ArrayLists.afk.contains(p)){
				ArrayLists.afk.remove(p);
				p.sendMessage("§cVous n'êtes plus AFK.");
				if (p.isOp() || p.hasPermission("admin")) {
					p.setPlayerListName(ChatColor.RED + "[Admin] " + p.getName() + " §7[§a✔§7]");
				} else if (p.hasPermission("mod")) {					           
					p.setPlayerListName(ChatColor.GREEN + "[Modérateur] " + p.getName() + " §7[§a✔§7]");
				} else if (p.hasPermission("respmod")) {				            
					p.setPlayerListName(ChatColor.DARK_GREEN + "[RespMod] " + p.getName() + " §7[§a✔§7]");
				} else if (p.hasPermission("dev")) {
					p.setPlayerListName(ChatColor.YELLOW + "[Dev] " + p.getName() + " §7[§a✔§7]");
				}
			}else{
				ArrayLists.afk.add(p);
				p.sendMessage("§cVous êtes maintenant AFK.");
				if (p.isOp() || p.hasPermission("admin")) {
					p.setPlayerListName(ChatColor.RED + "[Admin] " + p.getName() + " §7[§c✘§7]");
				} else if (p.hasPermission("mod")) {					           
					p.setPlayerListName(ChatColor.GREEN + "[Modérateur] " + p.getName() + " §7[§c✘§7]");
				} else if (p.hasPermission("respmod")) {				            
					p.setPlayerListName(ChatColor.DARK_GREEN + "[RespMod] " + p.getName() + " §7[§c✘§7]");
				} else if (p.hasPermission("dev")) {
					p.setPlayerListName(ChatColor.YELLOW + "[Dev] " + p.getName() + " §7[§c✘§7]");
				}
			}
		}else{
			p.sendMessage(HELP_MESSAGE);
		}
		return true;

	}

}
