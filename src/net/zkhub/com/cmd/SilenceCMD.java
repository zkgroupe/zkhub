package net.zkhub.com.cmd;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import net.zkhub.com.Hub;
import net.zkhub.com.enums.Chat;

public class SilenceCMD implements CommandExecutor{

	private BukkitTask task; 
	private int count = 6;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		// TODO
		Player p = (Player) sender;
		
		if(!(sender instanceof Player)) return true;
		
		if(!(p.hasPermission("mod") || p.isOp())) return true;
		
		if(args.length == 0){
			if(Chat.isState(Chat.NORMAL)){
				p.sendMessage("�cLe chat vas �tre sous silence dans 5 secondes.");
				taskRuntime(p);
				
			}else{
				Chat.setChatMode(Chat.NORMAL);
				p.sendMessage("�aLe chat n'est plus sous Silence. Les Joueurs peuvent � pr�sent parler.");
			}
		}		
		return true;
	}
	
	
	//Task de d�compte.
	private void taskRuntime(Player p){
		task = Bukkit.getScheduler().runTaskTimer(Hub.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				count--;
				if ((count <= 5 && count != 0) || count == 6){
					p.sendMessage("�cLe chat vas �tre sous silence dans : " + count);
					p.playSound(p.getLocation(), Sound.NOTE_BASS_GUITAR, 10.0F, 10.0F);
				}
				
				if(count == 0){
					Bukkit.getScheduler().cancelAllTasks();
					count = 6;
					Chat.setChatMode(Chat.MUTED);
					for(Player pl : Bukkit.getOnlinePlayers()) pl.sendMessage("�6Silence : �2Le chat est maintenant r�duit au Silence.");
					p.sendMessage("�cLe chat est maintenant sous Silence. Seuls les Mod�rateurs peuvent parler.");
				}
				
			}
		}, 20, 20);
		
	}
}
