package net.zkhub.com.cmd;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import net.zkhub.com.Hub;

public class SlowCMD implements CommandExecutor{

	public static int slow = 0;
    private static BukkitTask task;

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if(!(sender instanceof Player)){
			Hub.log("Command executable only by players.");
			return true;
		}

		Player p = (Player)sender;

		if(!(p.hasPermission("mod") || p.isOp())) return true;

		if(args.length == 0){
			p.sendMessage("Configuration du Slow, 1 message toutes les " + slow + " seconde(s)");
			return true;
		}

		if(args.length > 0){

			try{
				int time = Integer.parseInt(args[0]);
				
				if(time < 0 || time > 300){
					p.sendMessage("La valeur doit �tre comprise entre 0 et 300 secondes.");
					return true;
				}
				
				if(time == 0){
					Bukkit.broadcastMessage("�cSlow : �2D�sactiv�, les joueurs peuvent � pr�sent parler dans le chat sans d�lai.");
				}
				
				slow = time;
				Bukkit.broadcastMessage("�6Slow : �2Un message toutes les �e" + slow + " �2seconde(s)");
				runSilenceOffTask();
				
			}catch (Exception e){
				displayHelp(p);
				return true;
			}
		}

		return true;
	}

	private void displayHelp(Player p){
		p.sendMessage("�e/slow �fpour voir l'�tat du mode slow");
		p.sendMessage("�e/slow <secondes> �f1 messages toutes les X secondes.");
	}
	
	private void runSilenceOffTask(){
		if(task != null) task.cancel();
		task = Bukkit.getScheduler().runTaskLater(Hub.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				Bukkit.broadcastMessage("�6Slow : �2D�sactiv�, les joueurs peuvent � pr�sent parler dans le chat sans d�lai.");
			}
		}, 20 * 60 * 10);
	}
}


