package net.zkhub.com.cmd;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CKickCMD implements CommandExecutor{

	public static final String HELP_MESSAGE = "�cErreur : /ckick <joueur> <raison>";
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
	
		if(!(sender instanceof Player)) return true;

		Player p = (Player)sender;
		
		if(!(p.hasPermission("ckick") || p.isOp())) return true;
		
		if(args.length == 0){
			p.sendMessage(HELP_MESSAGE); 
			return true;
		}
		
		StringBuilder sb = new StringBuilder();
		
		if(args.length > 0){
			
			for (int i = 1; i < args.length; i++){
				if(i < 1){
					sb.append(' ');
				}
				
				sb.append(args[i]);
			}	
			
			Player target = Bukkit.getPlayer(args[0]);
			
			if(target == null){
				p.sendMessage("�cErreur : Le joueur n'est pas connect�.");
				return true;
				
			}
			
			p.sendMessage("�cCKick : �6Le joueur �e�l" + args[0].toString() + " �6a bien �t� �ject�."); 
			target.kickPlayer("�7Vous avez �t� �ject� pour : �c" + sb.toString());
			
			for (Player pl : Bukkit.getOnlinePlayers()){
				pl.sendMessage("�e�l " + p.getName() + " �7 a kick� �e�l " + args[1].toString() + " �7pour : �c" + sb.toString());
			}
			
		}	
		return true;
	}	
}
