package net.zkhub.com.cmd;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.zkhub.com.Hub;
import net.zkhub.com.utils.ArrayLists;

public class VanishCMD implements CommandExecutor{

	private static final String HELP_MESSAGE = "�cErreur : "
			+ "Vanish �aON/�cOFF"
			+ " �aPour l'activer, faites la commande sans arguments."
			+ " �cPour le d�sactiver faites de m�me.";

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(!(sender instanceof Player)){
			Hub.log("Command executable only by players");
		}

		Player p = (Player)sender;

		if(!(p.hasPermission("mod") || p.isOp())) return true;

		if(args.length == 0){


			if(ArrayLists.vanished.contains(p)){
				ArrayLists.vanished.remove(p);
				p.sendMessage("�cVous �tes visible aux yeux de tous.");
				for(Player pl : Bukkit.getOnlinePlayers()){
					pl.showPlayer(p);
				}
			}else{
				ArrayLists.vanished.add(p);
				p.sendMessage("�cVous �tes invisible aux yeux de tous.");
				for(Player pl : Bukkit.getOnlinePlayers()){
					if(!(pl.hasPermission("mod") || pl.isOp())){
						pl.hidePlayer(p);
					}
				}
			}
		}else{
			p.sendMessage(HELP_MESSAGE);
		}
		return true;
	}

}
