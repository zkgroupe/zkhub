package net.zkhub.com.cmd;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ModCMD implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		// TODO
		if(!(sender instanceof Player)){
			return true;
		}
		
		Player p = (Player)sender;
		
		if(!(p.hasPermission("mod") || p.isOp())){
			return true;
		}
		
		if(args.length == 0){
			p.sendMessage("�cUsage : /mod <text>");
			return true;
		}
		
		StringBuilder sb = new StringBuilder();
		
		for(int i = 0; i < args.length; i++){
			if(i > 0){
				sb.append(' ');
			}
			
			sb.append(args[i]);
		}
		
		for(Player pl : Bukkit.getOnlinePlayers()){
			pl.sendMessage("�6�l[MODERATION] " + p.getName() + " �f: " + sb.toString().replace("&", "�"));
		}
		
		
		return true;
	}

}
