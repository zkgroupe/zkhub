package net.zkhub.com.cmd;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.zkhub.com.sanctions.BanPlayer;

public class CBanCMD implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		
		if(!(sender instanceof Player)) return true;
		
		Player p = (Player)sender;
		
		if(!(p.hasPermission("mod") || p.isOp())) return true;
		
		if(args.length == 2){
			
			Player target = Bukkit.getPlayer(args[0]);
			
			StringBuilder sb = new StringBuilder();
			
			for(int i = 1; i < args.length; i++){
				if(i > 1){
					sb.append(' ');
				}
				
				sb.append(args[i]);
			}
			
			if(target != null){
				
				BanPlayer.ban(sb.toString(), p.getPlayer(), target.getPlayer(), args[1]);
				
			}else{
				p.sendMessage("�cErreur : Le joueur n'est pas connect�.");
			}
			
		}else{
			displayHelp(p);
		}
		
		
		
		return true;
	}
	
	private void displayHelp(Player p){
		
	}

}
