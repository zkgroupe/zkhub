package net.zkhub.com.villager;

import java.lang.reflect.Method;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftVillager;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Villager;
import net.minecraft.server.v1_8_R3.EntityVillager;
import net.minecraft.server.v1_8_R3.NBTTagCompound;

public class AiUtilsVillager {

public AiUtilsVillager(){
		
	}
	
    private static String serverVersion;
    private static Method getHandle;
    private static Method getNBTTag;
    private static Class<?> nmsEntityClass;
    private static Class<?> nbtTagClass;
    private static Method c;
    private static Method setInt;
    private static Method f;
 
    public static void setAiEnabled(Entity entity, boolean enabled) {
        try {
            if (serverVersion == null) {
                String name = Bukkit.getServer().getClass().getName();
                String[] parts = name.split("\\.");
                serverVersion = parts[3];
            }
            if (getHandle == null) {
                Class<?> craftEntity = Class.forName("org.bukkit.craftbukkit." + serverVersion + ".entity.CraftEntity");
                getHandle = craftEntity.getDeclaredMethod("getHandle");
                getHandle.setAccessible(true);
            }
            Object nmsEntity = getHandle.invoke(entity);
            if (nmsEntityClass == null) {
                nmsEntityClass = Class.forName("net.minecraft.server." + serverVersion + ".Entity");
            }
            if (getNBTTag == null) {
                getNBTTag = nmsEntityClass.getDeclaredMethod("getNBTTag");
                getNBTTag.setAccessible(true);
            }
            Object tag = getNBTTag.invoke(nmsEntity);
            if (nbtTagClass == null) {
                nbtTagClass = Class.forName("net.minecraft.server." + serverVersion + ".NBTTagCompound");
            }
            if (tag == null) {
                tag = nbtTagClass.newInstance();
            }
            if (c == null) {
                c = nmsEntityClass.getDeclaredMethod("c", nbtTagClass);
                c.setAccessible(true);
            }
            c.invoke(nmsEntity, tag);
            if (setInt == null) {
                setInt = nbtTagClass.getDeclaredMethod("setInt", String.class, Integer.TYPE);
                setInt.setAccessible(true);
            }
            int value = enabled ? 0 : 1;
            setInt.invoke(tag, "NoAI", value);
            if (f == null) {
                f = nmsEntityClass.getDeclaredMethod("f", nbtTagClass);
                f.setAccessible(true);
            }
            f.invoke(nmsEntity, tag);
        } catch (Exception e) {
            e.printStackTrace();
        }
    } 
    
    public static void createVillager(Location loc, String name){
		Villager villager = ((Villager) Bukkit.getWorld("world").spawnEntity(loc, EntityType.VILLAGER));
		CraftVillager craftVillager = ((CraftVillager) villager);
		EntityVillager entVillager = craftVillager.getHandle();	    
		Entity villag = entVillager.getBukkitEntity();
		villager.setAdult();
		setAiEnabled(villag, false);
		entVillager.getControllerLook().a(loc.getX(), loc.getY(), loc.getZ(), 10F, 40F);
		villag.setCustomName(name);
		villag.setCustomNameVisible(false);
		ArmorStand armor = (ArmorStand) Bukkit.getWorld("world").spawnEntity(loc.add(0, 1, 0), EntityType.ARMOR_STAND);
		armor.setCustomName("�e� �aClique droit");
		armor.setGravity(false);
		armor.setVisible(false);
		armor.setCustomNameVisible(true);
		ArmorStand armor2 = (ArmorStand) Bukkit.getWorld("world").spawnEntity(loc.add(0, 0.8, 0), EntityType.ARMOR_STAND);
		armor2.setCustomName(name);
		armor2.setGravity(false);
		armor2.setVisible(false);
		armor2.setCustomNameVisible(true);
		villag.setPassenger(armor);
		setAiEnabled(armor, false);
    }
	
	public static void setNoAI(Entity e) {
        net.minecraft.server.v1_8_R3.Entity nms = ((CraftEntity) e).getHandle();
        NBTTagCompound tag = new NBTTagCompound();
        nms.c(tag);
        tag.setInt("NoAI", 1);
        nms.f(tag);
        nms.h(true);
    }
	
}
