package net.zkhub.com.events.gui;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

import net.zkhub.com.boutique.Particle;
import net.zkhub.com.gui.GameGUI;

public class ClickEvent implements Listener {


	@EventHandler
	public void onClick(InventoryClickEvent e){

		Player p = (Player) e.getWhoClicked();

		if (e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR
				|| !e.getCurrentItem().hasItemMeta()) {
			return;
		}

		if(e.getCurrentItem().getType() == Material.COMPASS){
			GameGUI.openMenu(p);
		}
		
	}
}
