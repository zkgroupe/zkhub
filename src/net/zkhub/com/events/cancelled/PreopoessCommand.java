package net.zkhub.com.events.cancelled;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import com.comphenix.protocol.ProtocolLibrary;

public class PreopoessCommand implements Listener{
	
	@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
	public void onPlayerCommand(PlayerCommandPreprocessEvent event){
		
		Player player = event.getPlayer();
		
		if(event.getMessage().startsWith("/me")){
			event.setCancelled(true);
		}
		
		if(event.getMessage().startsWith("/tell")){
			event.setCancelled(true);
		}
	}

}
