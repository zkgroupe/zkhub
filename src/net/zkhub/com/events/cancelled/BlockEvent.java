package net.zkhub.com.events.cancelled;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockEvent implements Listener{
	
	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		Player p = (Player) e.getPlayer();
		if(p.hasPermission("admin") || p.isOp()){
			e.setCancelled(false);
			} else {
				e.setCancelled(true);
			}
	}

	@EventHandler
	public void onPlace(BlockPlaceEvent e) {
		Player p = (Player) e.getPlayer();
		if(p.hasPermission("admin") || p.isOp()){
			e.setCancelled(false);
			} else {
				e.setCancelled(true);
			}
	}

}
