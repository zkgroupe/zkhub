package net.zkhub.com.events.cancelled;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

import net.zkhub.com.utils.ArrayLists;
import net.zkhub.com.utils.Fonctions;

public class ExtraEvent implements Listener{
	
	@EventHandler
    public void onPlayerDamageEvent(EntityDamageEvent e){
       Player p = (Player)e.getEntity();
       
        if(e.getCause() != DamageCause.VOID){
            e.setCancelled(true);
        }else{
            e.setCancelled(true);
            Location spawn = new Location(Bukkit.getWorlds().get(0), 150, 179, -127);
            p.teleport(spawn);
            p.sendMessage("§7§oNe partez pas trop loin !");

        }
    }

	@EventHandler
    public void onPlayerEatChange(FoodLevelChangeEvent e){
        if(e.getEntity().getType().name() != null){
            e.setFoodLevel(20);
        }
    }
    
    @EventHandler
    public void onMove(PlayerMoveEvent e){
    	
    	Player p = e.getPlayer();
    	
    	if(ArrayLists.afk.contains(p)){
    		
    		p.sendMessage("§cVous n'êtes plus AFK");
    		
    		if (p.isOp() || p.hasPermission("admin")) {
				p.setPlayerListName(ChatColor.RED + "[Admin] " + p.getName() + " §7[§a✔§7]");
			} else if (p.hasPermission("mod")) {					           
				p.setPlayerListName(ChatColor.GREEN + "[Modérateur] " + p.getName() + " §7[§a✔§7]");
			} else if (p.hasPermission("respmod")) {				            
				p.setPlayerListName(ChatColor.DARK_GREEN + "[RespMod] " + p.getName() + " §7[§a✔§7]");
			} else if (p.hasPermission("dev")) {
				p.setPlayerListName(ChatColor.YELLOW + "[Dev] " + p.getName() + " §7[§a✔§7]");
			}
    		
    		ArrayLists.afk.remove(p);
    	}
    	
    	if(p.getLocation().getY() <= 3){
			p.teleport(new Location(Bukkit.getWorld("world"), -231.5, 133, 97.5, -90, 0));
			p.sendMessage("§7§oNe partez pas trop loin !");
			Fonctions.playSound(p, Sound.BAT_DEATH);
		}
    }
    
    @SuppressWarnings("deprecation")
	public void onPopup(EntityDamageByEntityEvent e) {
		if (e.getEntity().getType().equals(EntityType.PLAYER)) {
			Player p = (Player) e.getEntity();
			Player poper = (Player) e.getDamager();
			if (e.getDamager().getType() == EntityType.PLAYER) {
					p.playEffect(p.getLocation().add(0, 2, 0), Effect.LAVA_POP, 10);
					poper.playEffect(p.getLocation().add(0, 2, 0), Effect.LAVA_POP, 10);
					p.playSound(p.getLocation(), Sound.CHICKEN_EGG_POP, 1, 1);
					poper.playSound(poper.getLocation(), Sound.CHICKEN_EGG_POP, 1, 1);
					e.setCancelled(true);
					p.setVelocity(poper.getLocation().getDirection().multiply(1).add(new Vector(0, 1, 0)));
			}
		} else {
			e.setCancelled(true);
		}
	}
    
    @EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		e.setCancelled(true);
	}
	
	@EventHandler
    public void onPlayerMove(PlayerMoveEvent ev){
        Player p = ev.getPlayer();
        Block block = p.getLocation().getBlock().getRelative(BlockFace.DOWN);

        if (block.getType() == Material.SEA_LANTERN){
        	p.playSound(p.getLocation(), Sound.BAT_TAKEOFF, 1, 4);
        	p.setVelocity(p.getLocation().getDirection().multiply(3).add(new Vector(0, 2, 0)));
        	p.playSound(p.getLocation(), Sound.BAT_TAKEOFF, 1, 3);
        }
    }
}
