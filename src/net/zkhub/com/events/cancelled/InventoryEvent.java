package net.zkhub.com.events.cancelled;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryEvent implements Listener{
	
	@EventHandler
	public void isInventoryOpen(InventoryClickEvent e){
	
		Player p =(Player) e.getWhoClicked();
		
		if(p.getOpenInventory().getTitle() != null){
			if(!(p.hasPermission("builder") || p.isOp())){
				if(e.getAction() != null || e.getCurrentItem().getType() != Material.AIR){
					e.setCancelled(true);
				}
			}else{
				e.setCancelled(false);
			}
		}
	}
}
