package net.zkhub.com.events.accept;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import net.zkhub.com.Hub;
import net.zkhub.com.cmd.SlowCMD;
import net.zkhub.com.enums.Chat;
import net.zkhub.com.utils.MessagesUtils;
import net.zkhub.com.utils.SendTitle;

public class ChatEvent implements Listener{

	private int flood = 2;
	private ArrayList<String> lastWord = new ArrayList<>();
	private Map<String, Long> lastSpeak = new HashMap<>();
	private ArrayList<String> censured = new ArrayList<>();

	@EventHandler
	void onChat(AsyncPlayerChatEvent e){
		Player p = e.getPlayer();

		if (p.isOp() || p.hasPermission("admin")) {
			e.setFormat(ChatColor.RED + "[Admin]" + MessagesUtils.espace + p.getName() + MessagesUtils.espace + ChatColor.WHITE + ": " + e.getMessage());
		} else if (p.hasPermission("respmod")) {
			e.setFormat(ChatColor.DARK_GREEN + "[Resp.Mod]" + MessagesUtils.espace + p.getName() + MessagesUtils.espace + ChatColor.WHITE + ": " + e.getMessage());
		} else if (p.hasPermission("Ami")) {
			e.setFormat(ChatColor.WHITE + "Ami �3�f" + MessagesUtils.espace + p.getName() + MessagesUtils.espace + ChatColor.WHITE + ": " + e.getMessage());
		} else if (p.hasPermission("dev")) {
			e.setFormat(ChatColor.YELLOW + "[D�veloppeur]" + MessagesUtils.espace + p.getName() + MessagesUtils.espace + ChatColor.WHITE + ": " + e.getMessage());
		} else if (p.hasPermission("mod")) {
			e.setFormat(ChatColor.GREEN + "[Mod�rateur]" + MessagesUtils.espace + p.getName() + MessagesUtils.espace + ChatColor.WHITE + ": " + e.getMessage());
		} else {
			if(Chat.isState(Chat.MUTED)){
				e.setCancelled(true);
				p.sendMessage("�cLe chat est sous silence. Attendez qu'un mod�rateur le d�bloque.");
			}

			if(SlowCMD.slow > 0){
				Long lastSpeakingStamp = lastSpeak.get(p.getName());
				if(lastSpeakingStamp == null) lastSpeakingStamp = 0L;
				long result = System.currentTimeMillis() - lastSpeakingStamp;
				if(result < (SlowCMD.slow * 1000)){
					p.sendMessage("�6�oLe chat est sous Slow, un message toutes les " + SlowCMD.slow + " seconde(s)");
					e.setCancelled(true);
				}

				lastSpeak.put(p.getName(), System.currentTimeMillis());
			}

			e.setFormat("�7" + p.getName() + MessagesUtils.espace + ": " + e.getMessage());
		}

		//Censured control

		censured.add("pd");
		censured.add("fdp");
		censured.add("encul�");
		censured.add("ez");
		censured.add("fuck");
		censured.add("shit");
		censured.add("pute");
		censured.add("ta m�re");	
		censured.add("fils de chien");
		censured.add("tooleraor");
		censured.add("bais�");
		censured.add("baiser");
		censured.add("enculer");
		censured.add("sal");
		censured.add("izi");
		censured.add("salope");
		censured.add("tm");
		censured.add("vas te faire foutre");
		censured.add("vtf");
		censured.add("noob");
		censured.add("bite");
		censured.add("t�te de bite");
		censured.add("connard");
		censured.add("merde");
		censured.add("putain");
		censured.add("ptin");
		
		String message = e.getMessage();
		String[] words = message.split(" ");
		ArrayList<String> list = new ArrayList<String>();

		for(String word : words){
			for(String c : censured){
				if(word.equalsIgnoreCase(c)){
					e.setCancelled(true);
					SendTitle.sendTitle(p, "�cVotre mot a �t� censur�", "�2Merci de rester courtois avec les autres joueurs", 75);
				}
			}

			if(word.length() <= 50){
				list.add(word);
			}

		}

		String final_message = String.join("", list);

		for(String c : censured){
			if(final_message.equalsIgnoreCase(c)){
				e.setCancelled(true);
				SendTitle.sendTitle(p, "�cVotre mot a �t� censur�", "�2Merci de rester courtois avec les autres joueurs", 75);
			}
		}
	}
}
