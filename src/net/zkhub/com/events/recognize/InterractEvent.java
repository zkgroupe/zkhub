package net.zkhub.com.events.recognize;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import net.zkhub.com.gui.GameGUI;
import net.zkhub.com.gui.ProfilGUI;
import net.zkhub.com.gui.boutique.BoutiqueGUI;

public class InterractEvent implements Listener{

	@EventHandler
	public void onInteract(PlayerInteractEvent e){

		Player p = e.getPlayer();

		if(p.getItemInHand().getType() == Material.COMPASS){
			if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK){
				GameGUI.openMenu(p);
			}
		}
		
		if(p.getItemInHand().getType() == Material.SKULL_ITEM){
			if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK){
				ProfilGUI.openProfil(p);
			}
		}
		
		if(p.getItemInHand().getType() == Material.GOLD_INGOT){
			if(e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK || e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK){
				BoutiqueGUI.openBoutique(p);
			}
		}
	}
}
