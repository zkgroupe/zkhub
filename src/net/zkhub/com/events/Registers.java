package net.zkhub.com.events;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;

import net.zkhub.com.Hub;
import net.zkhub.com.events.accept.ChatEvent;
import net.zkhub.com.events.cancelled.BlockEvent;
import net.zkhub.com.events.cancelled.ExtraEvent;
import net.zkhub.com.events.cancelled.InventoryEvent;
import net.zkhub.com.events.cancelled.PreLoginEvent;
import net.zkhub.com.events.cancelled.PreopoessCommand;
import net.zkhub.com.events.cancelled.WeatherEvent;
import net.zkhub.com.events.gui.ClickEvent;
import net.zkhub.com.events.recognize.InterractEvent;
import net.zkhub.com.gui.GameGUI;
import net.zkhub.com.gui.ProfilGUI;
import net.zkhub.com.gui.boutique.BoutiqueGUI;
import net.zkhub.com.gui.boutique.ParticleGUI;
import net.zkhub.com.listeners.Join;
import net.zkhub.com.listeners.Quit;

public class Registers {
	
	public static void registersEvents(Plugin pl){
		
		PluginManager pm = Hub.getInstance().getServer().getPluginManager();
		
		pm.registerEvents(new Join(), pl);
		pm.registerEvents(new Quit(), pl);
		pm.registerEvents(new ChatEvent(), pl);
		pm.registerEvents(new BlockEvent(), pl);
		pm.registerEvents(new ExtraEvent(), pl);
		pm.registerEvents(new ClickEvent(), pl);
		pm.registerEvents(new WeatherEvent(), pl);
		pm.registerEvents(new InterractEvent(), pl);
		pm.registerEvents(new InventoryEvent(), pl);
		pm.registerEvents(new PreLoginEvent(), pl);
		pm.registerEvents(new PreopoessCommand(), pl);
		
		//REGISTER GUI
		pm.registerEvents(new GameGUI(), pl);
		pm.registerEvents(new ProfilGUI(), pl);	
		pm.registerEvents(new BoutiqueGUI(), pl);
		pm.registerEvents(new ParticleGUI(), pl);
	
	}
}
