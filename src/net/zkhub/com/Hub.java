package net.zkhub.com;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.plugin.java.JavaPlugin;

import net.zkhub.com.cmd.AfkCMD;
import net.zkhub.com.cmd.AnnonceCMD;
import net.zkhub.com.cmd.CBanCMD;
import net.zkhub.com.cmd.CKickCMD;
import net.zkhub.com.cmd.SilenceCMD;
import net.zkhub.com.cmd.ModCMD;
import net.zkhub.com.cmd.SlowCMD;
import net.zkhub.com.cmd.VanishCMD;
import net.zkhub.com.enums.Chat;
import net.zkhub.com.events.Registers;
import net.zkhub.com.utils.ArrayLists;

public class Hub extends JavaPlugin{
	
	public static Hub instance;
	
	@Override
	public void onEnable() {
		// TODO 
		
		instance = this;
		
		Registers.registersEvents(this);
		
		//COMMAND REGISTER
		this.getCommand("mod").setExecutor(new ModCMD());
		this.getCommand("annonce").setExecutor(new AnnonceCMD());
		this.getCommand("ckick").setExecutor(new CKickCMD());
		this.getCommand("cban").setExecutor(new CBanCMD());
		this.getCommand("afk").setExecutor(new AfkCMD());
		this.getCommand("vanish").setExecutor(new VanishCMD());
		this.getCommand("silence").setExecutor(new SilenceCMD());
		this.getCommand("slow").setExecutor(new SlowCMD());
		
		
		Bukkit.getWorld("world").setPVP(true);
		
		Location loc = new Location(Bukkit.getWorld("world"), -213.5, 129, 97.5);
		
		double phi = 0;
		phi = phi + Math.PI / 8;
		
		double x, y, z;
		
		for (double t = 0; t <= 0.1 * Math.PI; t = t = Math.PI / 1){
			for (double i = 0; i <= 1; i = i + 1){
				
				x = 0.1 * (1 * Math.PI - t) * 0.1 * Math.cos(t + phi + i * Math.PI);
				y = 0.1 + t;
				z = 0.1 * (1 * Math.PI - t) * 0.1 * Math.sin(t + phi + i * Math.PI);
				
				loc.add(x, y, z);
				loc.subtract(x, y, z);
				
				if (phi > 1 * Math.PI) {
					
					loc.add(x, y, z);
				}
			}
		}	
	}
	
	@Override
	public void onDisable() {
		// TODO 
		
		//TEST GIT
		
	}
	
	
	
	public static Hub getInstance(){
		return instance;
	}
	
	public static void log(String a){
		System.out.println(a);
	}

}
