package net.zkhub.com.utils;

import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle.EnumTitleAction;

public class SendTitle {

	public static void sendTitle(Player p, String title, String subtitle, int i){
		IChatBaseComponent titleComp = ChatSerializer.a("{\"text\": \""+title+"\"}");
		IChatBaseComponent subTitleComp = ChatSerializer.a("{\"text\": \""+subtitle+"\"}");

		PacketPlayOutTitle titlePacket = new PacketPlayOutTitle(EnumTitleAction.TITLE, titleComp);
		PacketPlayOutTitle subTitlePacket = new PacketPlayOutTitle(EnumTitleAction.SUBTITLE, subTitleComp);

		((CraftPlayer)p).getHandle().playerConnection.sendPacket(titlePacket);
		((CraftPlayer)p).getHandle().playerConnection.sendPacket(subTitlePacket);

		PacketPlayOutTitle timePacket = new PacketPlayOutTitle(EnumTitleAction.TIMES, null, 20 , i, 20);
		((CraftPlayer) p).getHandle().playerConnection.sendPacket(timePacket);
	}

}
