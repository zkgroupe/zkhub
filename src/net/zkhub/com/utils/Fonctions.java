package net.zkhub.com.utils;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class Fonctions {

	public static int getOnline(int port) {
		try {
			@SuppressWarnings("resource")
			Socket socket = new Socket();
			socket.connect(new InetSocketAddress("localhost", port), 1 * 1000);

			DataOutputStream out = new DataOutputStream(socket.getOutputStream());
			DataInputStream in = new DataInputStream(socket.getInputStream());

			out.write(0xFE);

			StringBuilder str = new StringBuilder();

			int b;
			while ((b = in.read()) != -1) {
				if (b != 0 && b > 16 && b != 255 && b != 23 && b != 24) {
					str.append((char) b);
				}
			}

			String[] data = str.toString().split("�");
			int onlinePlayers = Integer.valueOf(data[1]);
			return onlinePlayers;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
	
	public static ItemStack setItem(Material material, int number, String name, String desc){
        ItemStack item = new ItemStack(material, number);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        meta.setLore(Arrays.asList(desc));
        item.setItemMeta(meta);

        return item;
    }

    public static  void addItemInInventory(Inventory inv, Material material, int number, String name, String desc){
        ItemStack item = new ItemStack(material, number);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        meta.setLore(Arrays.asList(desc));
        item.setItemMeta(meta);
        inv.addItem(item);
    }

    public static void playSound(Player p, Sound s){
    	p.playSound(p.getLocation(), s, 1, 1);
    }
    
    public static ItemStack setSkull(int number, String owner, String name){
        ItemStack item = new ItemStack(Material.SKULL_ITEM, number, (short) SkullType.PLAYER.ordinal());
        SkullMeta meta = (SkullMeta) item.getItemMeta();
        meta.setDisplayName(name);
        meta.setOwner(owner);
        item.setItemMeta(meta);

        return item;
    }
	
}
