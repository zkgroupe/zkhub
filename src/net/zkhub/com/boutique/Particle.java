package net.zkhub.com.boutique;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import net.zkhub.com.utils.ArrayLists;

public class Particle {
	
	final static String lost_particle = "�cVotre particule a �t� supprim�e.";
	final static String have_particle = "�aVous poss�dez la particule �e%type%.";
	
	/**
	 * Liste des particules:
	 * 
	 * 
	 * 
	 * @param p
	 * @param particle
	 */
	public final static void setPlayerParticle(Player p, ParticleEffect particle){

		if (ArrayLists.particle.containsKey(p)) {
			
			p.sendMessage(lost_particle);
			ArrayLists.particle.remove(p);
			return;
			
		}else{
			
			for(Player pl : Bukkit.getOnlinePlayers()){
				particle.display(3, 3, 3, 10, 150, p.getEyeLocation(), pl);
			}
		
			
            p.sendMessage(have_particle.replaceAll("%type%", particle.toString().replaceAll("LAVA", "Lave")));
            ArrayLists.particle.put(p, particle);
            
            
		}
			
	}
	
}
