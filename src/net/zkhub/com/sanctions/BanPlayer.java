package net.zkhub.com.sanctions;

import org.bukkit.BanList;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Player;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.util.Vector;

import com.comphenix.protocol.ProtocolLib;

import net.zkhub.com.Hub;

public class BanPlayer {
	
	public static boolean banned = false;
	
	public static void ban(String reason, Player sender, Player p, String args){
		
		//Animation
		
		if(p != null){
			
			final Location spawn = p.getEyeLocation();
			
			p.getWorld().strikeLightningEffect(spawn);
			p.getWorld().playSound(spawn, Sound.ENDERDRAGON_GROWL, 100, 1);
			for(int i = 0; i < 3; i++){
				Bukkit.getScheduler().runTaskLater(Hub.getInstance(), new Runnable() {
					
					@Override
					public void run() {
						spawn.add(Vector.getRandom());
						Firework f = (Firework) p.getWorld().spawn(spawn, Firework.class);
						
						FireworkMeta fm = f.getFireworkMeta();
						fm.addEffect(FireworkEffect.builder()
								.flicker(false)
								.trail(true)
								.with(Type.STAR)
				
								.withColor(Color.BLACK, Color.ORANGE, Color.BLUE)
								.withFade(Color.GREEN)
								.build());
						
						fm.setPower(1);
						f.setFireworkMeta(fm);
					}
				}, 3 * i + 1);
			}
		
			p.kickPlayer("�7Vous avez �t� d�connect�, motif : �c�l" + args);
			Bukkit.getBanList(BanList.Type.NAME).addBan(p.getName(), reason, null, sender.getName());
			Bukkit.broadcastMessage("�f[�c�liProtector�f] �e" + p.getDisplayName() + " �7 a �t� banni pour �c�l" + reason);
				
			
			
		}
	}

}
