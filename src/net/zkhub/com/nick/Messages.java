package net.zkhub.com.nick;

import org.bukkit.ChatColor;

public class Messages {

	private static final String PREFIX = ChatColor.YELLOW + "[" + ChatColor.RED + "NMN" + ChatColor.YELLOW + "] ";
	public static final String NICK_SET = PREFIX + ChatColor.WHITE + "Your username was defined : " + ChatColor.RED + ChatColor.BOLD.toString() + "$username";
	public static final String NICK_UNSET = PREFIX + ChatColor.WHITE + "Your username was reset !";

	public static String formatUsername(String format, String username){
		String message = format;
		return message.replace("$username", username);
	}

}
