package net.zkhub.com.waiting;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Properties;
import org.apache.commons.io.FileUtils;
import org.bukkit.entity.Player;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import net.zkhub.com.Hub;


public class WaitingUtils {

	public static HashMap<String, ByteArrayDataOutput> creates = new HashMap<String, ByteArrayDataOutput>();

	public static void create(String name, String game, int port){
		String path = "/home/files/"+name;
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		String portS = Integer.toString(port);
		out.writeUTF("FilesCreate");
		out.writeUTF(portS);
		out.writeUTF(name);
		new File(path).mkdirs();
		File srcDir = new File("/home/servertemplate");
		File destDir = new File(path);
		File worldDir = new File("/home/templates/"+game.toLowerCase()+"/");
		try {

			FileUtils.copyDirectory(worldDir, destDir);
		} catch (IOException e22){
			e22.printStackTrace();
		}

		try {
			FileUtils.copyDirectory(srcDir, destDir);
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			Properties props = new Properties();
			props.setProperty("online-mode", "false");
			props.setProperty("spawn-protection", "0");
			props.setProperty("announce-player-achievements", "false");
			props.setProperty("server-ip", "127.0.0.1");
			props.setProperty("server-port", ""+port);
			props.setProperty("motd", ""+name);
			File f = new File(path+"/server.properties");
			OutputStream outs = new FileOutputStream( f );
			props.store(outs, "Plugin cree par Oramni pour Syntro");
		}
		catch (Exception e ) {
			e.printStackTrace();
		}
		File ssh = new File(path+"/start.sh");
		ssh.setExecutable(true);
		ssh.setReadable(true);
		ssh.setWritable(true);
		try {
			@SuppressWarnings("unused")
			Process Pro = Runtime.getRuntime().exec("sh start.sh", null, new File(path));
		} catch (IOException e) {
			e.printStackTrace();
		}
		creates.put(name, out);
	}



	public static void sendServer(Player p, String server) {
		ByteArrayDataOutput out = ByteStreams.newDataOutput();
		out.writeUTF("Connect");
		out.writeUTF(server);
		p.sendPluginMessage(Hub.getInstance(), "BungeeCord", out.toByteArray());
	}

}
